<?php
include_once 'internal_data/_settings.php';
include_once 'internal_data/_connect.php';
include_once 'internal_data/functions/coreFunctions.php';

$usersTable = $_SETTINGS['USERS_TABLE'];

if (isset($_SESSION['Username'])) {

    $username = $_SESSION['Username'];

    $errorMessage = null;
    $errorMessage2 = null;
    $errorMessage3 = null;

    if (isset($_POST['newPassword'])) {
        $currentPassword = hash("sha256", $_POST['currentPassword']);
        $currentPassword = stripslashes($currentPassword);
        $currentPassword = $connection->real_escape_string($currentPassword);

        $command = "SELECT * FROM $usersTable WHERE Username = '$username' AND Password = '$currentPassword';";
        $query = mysqli_query($connection, $command) or die(mysqli_error());
        print mysqli_fetch_array($query)[2];

        $correctPassword = mysqli_num_rows($query) == 1 ? true : false;

        if ($correctPassword) {
            $newPassword = hash("sha256", $_POST['newPassword']);
            $newPassword = stripslashes($newPassword);
            $newPassword = $connection->real_escape_string($newPassword);

            $command = "UPDATE $usersTable SET Password = '$newPassword' WHERE Username = '$username';";
            $query = mysqli_query($connection, $command) or die(mysqli_error());

            $errorMessage = "Password Set!";
        } else {
            $errorMessage = "Incorrect Password!";
        }
    } else if (isset($_POST['newUsername'])) {
        $currentPassword = hash("sha256", $_POST['currentPassword']);
        $currentPassword = stripslashes($currentPassword);
        $currentPassword = $connection->real_escape_string($currentPassword);

        $command = "SELECT * FROM $usersTable WHERE Username = '$username' AND Password = '$currentPassword';";
        $query = mysqli_query($connection, $command) or die(mysqli_error());

        $correctPassword = mysqli_num_rows($query) == 1 ? true : false;

        $newUsername = $_POST['newUsername'];

        $newUsername = stripslashes($newUsername);
        $newUsername = $connection->real_escape_string($newUsername);

        if ($correctPassword) {
            $command = "UPDATE $usersTable SET Username = '$newUsername' WHERE Username = '$username';";
            $errorMessage2 = "Username Changed!";
            $query = mysqli_query($connection, $command) or die($errorMessage2 = "Username Taken!");
        } else {
            $errorMessage2 = "Incorrect Password!";
        }
    } else if (isset($_POST['newUser'])) {
        $newUsername = $_POST['newUser'];
        $newUsername = stripslashes($newUsername);
        $newUsername = $connection->real_escape_string($newUsername);

        $Password = hash("sha256", $_POST['Password']);
        $Password = stripslashes($Password);
        $Password = $connection->real_escape_string($Password);
        
        $currentPassword = hash("sha256", $_POST['currentPassword']);
        $currentPassword = stripslashes($currentPassword);
        $currentPassword = $connection->real_escape_string($currentPassword);

        $command = "SELECT * FROM $usersTable WHERE Username = '$username' AND Password = '$currentPassword';";
        $query = mysqli_query($connection, $command) or die(mysqli_error());

        $correctPassword = mysqli_num_rows($query) == 1 ? true : false;

        if ($_POST['Administrator'] == 1) {
            if ($correctPassword) {
                $command = "INSERT INTO $usersTable (Username, Password, Administrator) VALUES ('$newUsername', '$Password', 1);";
                $errorMessage3 = "User Added!";
                $query = mysqli_query($connection, $command) or die($errorMessage3 = "Username Taken!");
            } else {
                $_POST['currentPassword'] == "" ? $errorMessage3 = "Password Required!" : $errorMessage3 = "Incorrect Password!";
            }
        } else {
            $command = "INSERT INTO $usersTable (Username, Password, Administrator) VALUES ('$newUsername', '$Password', 0);";
            $errorMessage3 = "User Added!";
            $query = mysqli_query($connection, $command) or die($errorMessage3 = "Username Taken!");
        }
    } else if(isset($_POST['DeleteUser'])) {
        $Password = hash("sha256", $_POST['Password']);
        $Password = stripslashes($Password);
        $Password = $connection->real_escape_string($Password);
        
        $command = "SELECT * FROM $usersTable WHERE Username = '$username' AND Password = '$Password';";
        $query = mysqli_query($connection, $command) or die(mysqli_error());

        $correctPassword = mysqli_num_rows($query) == 1 ? true : false;
        if($correctPassword){
            $userID = $_POST['userID'];
            $delUsername = $_POST['userName'];
            $command = "DELETE FROM $usersTable WHERE User_ID='$userID';";
            $errorMessage3 = "User $delUsername successfully removed!";
            $query = mysqli_query($connection, $command) or die($errorMessage3 = "Oops, something went wrong!");
        } else {
            $errorMessage3 = "Incorrect Password!";
        }
    }
} else {
    header("Location: index.php");
}
?>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" type="text/css" href="design/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="design/css/style.css">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="design/css/js/bootstrap.min.js"></script>

        <title>BARSC Admin</title>
    </head>
    <body>
        <?php
        print returnSidebar("admin");
        ?>
        <div class="content-container">
            <div class="main-content" style="text-align: center;">
                <h1 class="content-title">Administration</br>
                    <?php
                if ($errorMessage3 != null) {
                    ($errorMessage3 == "User Added!" || $errorMessage3 == "User $userID successfully removed!") ? print "<span class='label label-success' style='position: relative; top: 7px;'>$errorMessage3</span>"
                                     : print "<span class='label label-warning' style='position: relative; top: 7px;'>$errorMessage3</span>";
                }
                ?>
                </h1>

                <div class='inner-content-block'>
                    <h2 class='inner-content-title'>Change Password</h2>
                        <?php
                        if ($errorMessage != null) {
                            $errorMessage == "Password Set!" ? print "<span class='label label-success' style='position: relative; top: 7px;'>$errorMessage</span>"
                                             : print "<span class='label label-warning' style='position: relative; top: 7px;'>$errorMessage</span>";
                        }
                        ?>
                        <form class="form-signin" action="" method="post" name="changePassword">
                            <input class="form-control" style="margin: 10px; width: 380px;" type="password" name="newPassword" placeholder="New Password" required>
                            <input class="form-control" style="margin: 10px; width: 380px;" type="password" name="currentPassword" placeholder="Current Password" required>
                            <button style='margin-top: -2px;' class="btn btn-primary" type="submit">Change</button>
                        </form>
                </div>

                <div class='inner-content-block'>
                    <h2 class='inner-content-title'>Change Username</h2>
                        <?php
                        if ($errorMessage2 != null) {
                            $errorMessage2 == "Username Set!" ? print "<span class='label label-success' style='position: relative; top: 7px;'>$errorMessage2</span>"
                                             : print "<span class='label label-warning' style='position: relative; top: 7px;'>$errorMessage2</span>";
                        }
                        ?>
                        <form class="form-signin" action="" method="post" name="changeUsername">
                            <input class="form-control" style="margin: 10px; width: 380px;" type="text" name="newUsername" placeholder="New Username" required>
                            <input class="form-control" style="margin: 10px; width: 380px;" type="password" name="currentPassword" placeholder="Current Password" required>
                            <button style='margin-top: -2px;' class="btn btn-primary" type="submit">Change</button>
                        </form>
                </div>

                <?php
                if ($_SESSION['Rank'] == 1) {

                    $command = "SELECT * FROM $usersTable;";
                    $query = mysqli_query($connection, $command) or die(mysqli_error());

                    print "
                            <table class='table table-striped admin-table'>
                                <thead>
                                    <th class='styled'>User ID</th>
                                    <th class='styled'>Username</th>
                                    <th class='styled'>Administrator</th>
                                    <th class='styled'>-</th>
                                </thead>
                                <tbody>";
                    while ($row = mysqli_fetch_array($query)) {
                        $admin = $row[3] == 1 ? "<span class='glyphicon glyphicon-ok' style='color:green'></span>" : "<span class='glyphicon glyphicon-remove' style='color:red'></span>";
                        print "
                                        <tr>
                                            <td style='vertical-align: middle;'>$row[0]</td>"
                                . "<td style='vertical-align: middle;'>$row[1]</td>"
                                . "<td style='vertical-align: middle;'>$admin</td>"
                                . "<td style='vertical-align: middle;'><button class='btn btn-primary' data-toggle='modal' href='#delete$row[0]'>Remove</button></td>
                                        </tr>";
                        
                        print "<div class='modal fade' id='delete$row[0]' tabindex='-1' role='dialog' aria-labelledby='delete$row[0]' aria-hidden='true'>

            <div class='modal-dialog modal-sm'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                        <p class='modal-title'>Remove $row[1]?</p>
                    </div>
                    <div class='modal-body' style='text-align: left;'>
                        <form action='' method='post' name='deleteForm'>
                            <input type='hidden' name='userID' value='$row[0]'/>
                            <input type='hidden' name='userName' value='$row[1]'/>
                            <input class='form-control form-styling' type='password' name='Password' placeholder='Password' required>
                            <button class='btn btn-primary' style='margin: 10px 0px; width: 270px;' type='submit' name='DeleteUser'>Remove User?</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>";
                    }
                    print "
                                </tbody>
                            </table>";

                    print "<button class='btn btn-primary' style='position: absolute; width: 170px; bottom: 5px; right: 5px;' data-toggle='modal' href='#newUser'>New User</button>";
                } else {
                    print "<button class='btn btn-primary' style='position: absolute; width: 170px; bottom: 5px; right: 5px;' data-toggle='modal' href='' disabled>New User</button>";
                }
                ?>                
            </div>
        </div>

        <div class="modal fade" id="newUser" tabindex="-1" role="dialog" aria-labelledby="newUser" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <p class="modal-title">Add New User</p>
                    </div>
                    <div class="modal-body">
                        <form class="form-signin" action="" method="post" name="newUserForm">
                            <input class="form-control form-styling" type="text" name="newUser" placeholder="New Username" required>
                            <input class="form-control form-styling" type="password" name="Password" placeholder="New User's Password" required>
                            <script>
                                $(document).ready(function () {
                                    toggleFields();
                                    $('#Administrator').change(function () {
                                        toggleFields();
                                    });
                                });

                                function toggleFields() {
                                    if ($('#Administrator').val() == '1')
                                        $('#passwordDiv').show();
                                    else
                                        $('#passwordDiv').hide();
                                }
                            </script>
                            <select name='Administrator' id='Administrator' class='form-control form-styling'>
                                <option value="0">Non-Administrator</option>
                                <option value="1">Administrator</option>
                            </select>
                            <div id='passwordDiv'>
                                <input class='form-control form-styling' type='password' name='currentPassword' placeholder='Current Password'>
                            </div>
                            <button style='margin-top: -2px;' class="btn btn-primary" type="submit">Add</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="logout" tabindex="-1" role="dialog" aria-labelledby="logout" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <p class="modal-title">Are you sure you want to logout?</p>
                    </div>
                    <div class="modal-body">
                        <div style="text-align: center;">
                            <form method="post" action="internal_data/functions/logout.php">
                                <button class="btn btn-primary" type="submit" name="logout">Logout</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </body>
    <footer>



    </footer>
</html>