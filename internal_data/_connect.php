<?php
include_once('_settings.php');

    session_start();
    
    $connection = new mysqli(
            $_SETTINGS['SQL_ADDRESS'],
            $_SETTINGS['SQL_USERNAME'],
            $_SETTINGS['SQL_PASSWORD'],
            $_SETTINGS['SQL_DATABASE'],
            $_SETTINGS['SQL_PORT']) or die("Error: Unable to connect to database:" . PHP_EOL);
