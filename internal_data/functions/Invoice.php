<?php
include_once '../_settings.php';
include_once '../_connect.php';

$servicesTable = $_SETTINGS['SERVICES_TABLE'];
$customersTable = $_SETTINGS['CUSTOMERS_TABLE'];
$carsTable = $_SETTINGS['CARS_TABLE'];
$partsTable = $_SETTINGS['PARTS_TABLE'];

$serviceID = $_GET['serviceID'];
$totalPrice = 0.00;

$command = "SELECT $servicesTable.*, $customersTable.*, $carsTable.* FROM $servicesTable JOIN $customersTable ON "
        . "$servicesTable.Customer_ID = $customersTable.Customer_ID JOIN $carsTable ON $servicesTable.Car_ID = $carsTable.Car_ID"
        . " WHERE $servicesTable.Service_ID = '$serviceID';";

$query = mysqli_query($connection, $command) or die(mysqli_error());
$return = mysqli_fetch_array($query);

$type = $return[3] == "Other" ? "$return[3]: $return[4]" : $return[3];

$partsCommand = "SELECT * FROM $partsTable WHERE Service_ID = '$serviceID';";
$partsQuery = mysqli_query($connection, $partsCommand) or die(mysqli_error());
?>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" type="text/css" href="/design/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/design/css/style.css">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="/design/css/js/bootstrap.min.js"></script>
        <style>
            body {
                padding: 50px;
            }
        </style>
    </head>
    <body>
        <table class='table table-striped'>
            <thead>
                <tr>
                    <th colspan='4' class='styled'>Service Invoice: <?php print $return[0]; ?></th>
                </tr><tr>
                    <th class='styled'>ID</th>
                    <th class='styled'>Type</th>
                    <th class='styled'>Started</th>
                    <th class='styled'>Finished</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?php print $return[0]; ?></td>
                    <td><?php print $type; ?></td>
                    <td><?php print $return[5]; ?></td>
                    <td><?php print $return[6]; ?></td>
                </tr>
            </tbody>
        </table>

        <table class='table table-striped'>
            <thead>
                <tr>
                    <th colspan='5' class='styled'>Customer</th>
                </tr><tr>
                    <th class='styled'>ID</th>
                    <th class='styled'>Name</th>
                    <th class='styled'>Date of Birth</th>
                    <th class='styled'>Address</th>
                    <th class='styled'>Phone Number</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?php print $return[8]; ?></td>
                    <td><?php print $return[9]; ?></td>
                    <td><?php print $return[10]; ?></td>
                    <td><?php print $return[12].",</br>". ($return[13] != null ? $return[13].",</br>" : "") . "$return[15],</br> $return[16],</br> $return[14]"; ?></td>
                    <td><?php print $return[11]; ?></td>
                </tr>
            </tbody>
        </table>
        <table class='table table-striped'>
            <thead>
                <tr>
                    <th colspan='5' class='styled'>Car</th>
                </tr><tr>
                    <th class='styled'>ID</th>
                    <th class='styled'>Registration</th>
                    <th class='styled'>Make</th>
                    <th class='styled'>Model</th>
                    <th class='styled'>Colour</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?php print $return[17]; ?></td>
                    <td><?php print $return[19]; ?></td>
                    <td><?php print $return[20]; ?></td>
                    <td><?php print $return[21]; ?></td>
                    <td><?php print $return[22]; ?></td>
                </tr>
            </tbody>
        </table>

        <table class='table table-striped' style='margin-bottom: 50px;'>
            <thead>
                <tr>
                    <th colspan='3' class='styled'>Service Parts</th>
                </tr><tr>
                    <th class='styled'>ID</th>
                    <th class='styled'>Name</th>
                    <th class='styled'>Price</th>
                </tr>
            </thead>
            <tbody>
                <?php
                while ($partsRow = mysqli_fetch_array($partsQuery)) {
                    $totalPrice += $partsRow[2];
                    print "<tr>"
                            . "<td>$partsRow[0]</td>"
                            . "<td>$partsRow[1]</td>"
                            . "<td>£$partsRow[2]</td>"
                            . "</tr>";
                }
                ?>
            </tbody>
        </table>
        <span class='alert-success' style='float: right;'><?php
            setlocale(LC_MONETARY,"en_GB.UTF-8");
        
            print money_format("Total Price: %n", $totalPrice);
        ?></span>
    </body>
</html>