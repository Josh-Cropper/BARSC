<?php

include_once '../_settings.php';
include_once '../_connect.php';

$servicesTable = $_SETTINGS['SERVICES_TABLE'];
$customersTable = $_SETTINGS['CUSTOMERS_TABLE'];
$carsTable = $_SETTINGS['CARS_TABLE'];

function ongoingServicesTable() {
    global $connection, $servicesTable, $customersTable, $carsTable;

    $command = "SELECT $servicesTable.*, $customersTable.*, $carsTable.* FROM $servicesTable"
            . " JOIN $customersTable ON $servicesTable.Customer_ID = $customersTable.Customer_ID"
            . " JOIN $carsTable ON $servicesTable.Car_ID = $carsTable.Car_ID"
            . " WHERE $servicesTable.Service_Finished IS NULL"
            . " AND $servicesTable.Aborted_Reason IS NULL";
    
    $query = mysqli_query($connection, $command) or die(mysqli_error());

    $returnString = "<table class='table table-striped'>"
            . "<thead>"
            . "<th class='styled'>Service ID</th>"
            . "<th class='styled'>Customer</th>"
            . "<th class='styled'>Type</th>"
            . "<th class='styled'>Started</th>"
            . "<th class='styled'>More Info</th>"
            . "</thead>"
            . "<tbody>";

    while ($row = mysqli_fetch_array($query)) {
        $otherDetails = $row[3] == "Other" ? "$row[3]: $row[4]" : $row[3] ;
        $returnString .= "<tr style='height: 50px;'>"
                . "<td style='vertical-align: middle;'>$row[0]</td>"
                . "<td style='vertical-align: middle;'><form action='customers.php' method='post' style='margin-bottom: 0px;'>"
                . "<input type='hidden' name='searchValue' value='$row[9]'>"
                . "<button class='btn btn-primary' type='submit' name='submitButton'>$row[9]</button>"
                . "</form></td>"
                . "<td style='vertical-align: middle;'>$row[3]</td>"
                . "<td style='vertical-align: middle;'>$row[5]</td>"
                . "<td style='vertical-align: middle;'>"
                . "<button class='btn btn-primary' data-toggle='modal' href='#moreInfo$row[0]'>More Info</button>"
                . "<div class='modal fade' id='moreInfo$row[0]' tabindex='-1' role='dialog' aria-labelledby='moreInfo$row[0]' aria-hidden='true' >
                        <div class='modal-dialog modal-sm' style='width: 800px;'>
                            <div class='modal-content'>
                                <div class='modal-header'>
                                    <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                                    <p class='modal-title'>More Info: Open Service $row[0]</p>
                                </div>
                                <div class='modal-body' >
                                    <table class='table table-striped'>
                                        <thead>
                                            <tr>
                                                <th colspan='6' class='styled'>Customer</th>
                                            </tr><tr>
                                                <th class='styled'>ID</th>
                                                <th class='styled'>Name</th>
                                                <th class='styled'>Date of Birth</th>
                                                <th class='styled'>Address</th>
                                                <th class='styled'>Phone Number</th>
                                                <th class='styled'>Cars</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>$row[8]</td>
                                                <td>$row[9]</td>
                                                <td>$row[10]</td>
                                                <td>$row[12],</br>". ($row[13] != null ? "$row[13],</br>" : "") ."$row[15],</br> $row[16],</br> $row[14]</td>
                                                <td>$row[11]</td>
                                                <td><form action='cars.php' method='post'>"
                                                    . "<input type='hidden' name='searchValue' value='$row[9]'>"
                                                    . "<button class='btn btn-primary' type='submit' name='submitButton'>Find</button>"
                                                    . "</form></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class='table table-striped'>
                                        <thead>
                                            <tr>
                                                <th colspan='5' class='styled'>Car</th>
                                            </tr><tr>
                                                <th class='styled'>ID</th>
                                                <th class='styled'>Registration</th>
                                                <th class='styled'>Make</th>
                                                <th class='styled'>Model</th>
                                                <th class='styled'>Colour</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>$row[17]</td>
                                                <td style='text-transform: uppercase;'>$row[19]</td>
                                                <td>$row[20]</td>
                                                <td>$row[21]</td>
                                                <td>$row[22]</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class='table table-striped' style='margin-bottom: 40px;'>
                                        <thead>
                                            <tr>
                                                <th colspan='4' class='styled'>Service Info</th>
                                            </tr><tr>
                                                <th class='styled'>ID</th>
                                                <th class='styled'>Type</th>
                                                <th class='styled'>Started</th>
                                                <th class='styled'>Finished</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>$row[0]</td>
                                                <td>$otherDetails</td>
                                                <td>$row[5]</td>
                                                <td><form name='finishService' method='post' action=''>
                                                        <input type='hidden' name='serviceID' value='$row[0]'>
                                                        <button class='btn btn-primary' type='submit' name='finishButton'>Finish</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <form action='services.php' method='post' style='width: 120px; position: absolute; bottom: -10px; right: 5px;'>"
                                                    . "<input type='hidden' name='searchValue' value='$row[0]'>"
                                                    . "<button class='btn btn-primary' type='submit' name='submitButton'>Go To Service</button>"
                                                    . "</form>
                                </div>
                            </div>
                        </div>
                    </div>"
                . "</td>"
                . "</tr>";
    }

    $returnString .= "</tbody>"
            . "</table>";

    return $returnString;
}

function returnSidebarSearch() {
    return "<script>
        $(window).keydown(function(event){
    if((event.which== 13) && ($(event.target)[0]!=$('textarea')[0])) {
      event.preventDefault();
      return false;
    }
  });
  </script>
        
<form action='' method='post' name='searchForm' id='searchForm'>
                <input class='sidebar-search' type='text' name='searchValue' placeholder='Search' required>
                <input type='submit' style='display:none' name='submitButton'>
            </form>
            <button class='btn btn-primary' style='width: 70px; float: right; margin-top: -48px;' data-toggle='modal' href='#search'>Search</button>";
}

function returnSidebar($page, $type = "string") {
    $return = "<div class='sidebar-container'>
            <div class='logo-container-sidebar'>
                <h3 class='logo'>BARSC</h3>
            </div>" .
            returnSidebarSearch()
            . "<ul style='margin-top: 20px;' class='nav nav-pills nav-stacked'>";

    if ($page == "overview") {
        $return .= "<li class='active'><a href=''><strong>Overview</strong></a></li>
                <li><a href='services.php'><strong>Services</strong></a></li>
                <li><a href='customers.php'><strong>Customers</strong></a></li>
                <li><a href='cars.php'><strong>Cars</strong></a></li>
                <li><a href='admin.php'><strong>Administration</strong></a></li>";
    } else if ($page == "services") {
        $return .= "<li><a href='overview.php'><strong>Overview</strong></a></li>
                <li class='active'><a href=''><strong>Services</strong></a></li>
                <li><a href='customers.php'><strong>Customers</strong></a></li>
                <li><a href='cars.php'><strong>Cars</strong></a></li>
                <li><a href='admin.php'><strong>Administration</strong></a></li>";
    } else if ($page == "customers") {
        $return .= "<li><a href='overview.php'><strong>Overview</strong></a></li>
                <li><a href='services.php'><strong>Services</strong></a></li>
                <li class='active'><a href=''><strong>Customers</strong></a></li>
                <li><a href='cars.php'><strong>Cars</strong></a></li>
                <li><a href='admin.php'><strong>Administration</strong></a></li>";
    } else if ($page == "cars") {
        $return .= "<li><a href='overview.php'><strong>Overview</strong></a></li>
                <li><a href='services.php'><strong>Services</strong></a></li>
                <li><a href='customers.php'><strong>Customers</strong></a></li>
                <li class='active'><a href=''><strong>Cars</strong></a></li>
                <li><a href='admin.php'><strong>Administration</strong></a></li>";
    } else if ($page == "admin") {
        $return .= "<li><a href='overview.php'><strong>Overview</strong></a></li>
                <li><a href='services.php'><strong>Services</strong></a></li>
                <li><a href='customers.php'><strong>Customers</strong></a></li>
                <li><a href='cars.php'><strong>Cars</strong></a></li>
                <li class='active'><a href=''><strong>Administration</strong></a></li>";
    }
    $return .= "<li class='divider'></li>
                <li><a data-toggle='modal' href='#logout'><strong>Logout</strong></a></li>
                <li style='position: absolute; bottom: 10px; width: 220px;'><a data-toggle='modal' href='#help'><strong>Help</strong></a></li>
            </ul>"
        ."</div>"
            . help()
            ."<div class='modal fade' id='search' tabindex='-1' role='dialog' aria-labelledby='search' aria-hidden='true' >
                    <div class='modal-dialog modal-sm' style='width: 400px;'>
                        <div class='modal-content'>
                            <div class='modal-header'>
                                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                                <p class='modal-title'>Search For:</p>
                            </div>
                            <div class='modal-body' >
                                <div style='text-align: center;'>
                                <script type='text/javascript'>
                                    function OnSubmitForm(value) {
                                        document.searchForm.action = value;
                                        document.searchForm.submitButton.click();
                                    }
                                </script>
                                    <button class='btn btn-primary' onclick='OnSubmitForm(\"services.php\");'>Service</button>
                                    <button class='btn btn-primary' onclick='OnSubmitForm(\"customers.php\");'>Customer</button>
                                    <button class='btn btn-primary' onclick='OnSubmitForm(\"cars.php\");' value='cars'>Car</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>";

    return $return;
}


function createNewCustomerModal() {
    $return = "<div class='modal fade' id='createCustomer' tabindex='-1' role='dialog' aria-labelledby='createCust' aria-hidden='true' >
                                    <div class='modal-dialog modal-sm' style='width: 400px; z-index: 1151'>
                                        <div class='modal-content'>
                                            <div class='modal-header'>
                                                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                                                <p class='modal-title'>Create New Customer</p>
                                            </div>
                                            <div class='modal-body' >
                                                <form name='createCustomerForm'>
                                                    <input type='text' name='name' placeholder='John Doe'>
                                                    <input type='text' name='address' placeholder='123 RoadStreet, City, Postcode'>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>";
}

function createNewServiceModal($carID, $type = "int") {
    $return =
"<script>
            $(document).ready(function () {
            toggleFields();

            $('#serviceType').change(function () {
                toggleFields();
            });

        });

        function toggleFields() {
            if ($('#serviceType').val() == 'other')
                $('#other').show();
            else
                $('#other').hide();
                $('#other').val() = '';
        }
        </script>
        
        <div class='modal fade' id='createService' tabindex='-1' role='dialog' aria-labelledby='createServ' aria-hidden='true' >
            <div class='modal-dialog modal-sm' style='width: 400px;'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                        <p class='modal-title'>Create New Service</p>
                    </div>
                    <div class='modal-body' >
                        <form name='createServiceForm'>
                            <input type='hidden' name='carID' value='$carID'>
                            <select name='serviceType' id='serviceType'>
                                <option value='MOT'>MOT</option>
                                <option value='other'>Other</option>
                            </select>
                            <input type='text' name='other' id='other'>
                        </form>
                    </div>
                </div>
            </div>
        </div>";
    
    return $return;
}

function help() {
    $return = "<div class='modal fade' id='help' tabindex='-1' role='dialog' aria-labelledby='help' aria-hidden='true' >
            <div class='modal-dialog modal-sm' style='width: 800px;'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                        <p class='modal-title'>Help</p>
                    </div>
                    <div class='modal-body' >
                        <table class='table table-striped' style='text-align: left;'>
                            <thead>
                                <th class='styled'>Basic Functionality</th>
                            </thead>
                            <tbody>
                                <td><p>As a basic rule of thumb, the order in which a service should be set up, is as follows:</br></br>
                                Firstly, you should create a customer, the person that owns the car you are servicing. Don't worry, though, customers can own multiple cars, and can be edited at anytime, if you should happen to make a mistake in their creation. Simply go to the customers page, and click \"Create Customer\", you will then be greeted with a form to input their information.</br></br>
                                
Secondly you should create a car to be serviced. We have made this process as simple as possible, you simply click \"Create Car\", you can then search for a customer by name, then simply select them from a selection of customers that have names similar to what you searched. Then, you will be greeted with another form, to enter all the car's information. Again, you need not worry, as all the car's information is editable as simply is it was to create it.</br></br>

Finally, the service itself. Much like the creation of the car, upon selecting \"Create Service\", you will be greeted with a search box, to search for the car to be serviced, you can search either by registration or by customer. You again simply select the car from the results, and input the service details. After this you can add parts to the service, these are for records of what has been done to the car, and the cost> Labour and such can be included in this, not necessarily just parts. Anything you would want a record of and price for, for the final invoice.</br></br>

Services that are still ongoing will be displayed on the main dashboard, ordered by which have been ongoing for the longest, this is to help you keep track of which services need finishing most urgently. To finish a service, simply click \"More Info\" for the service on either the dashboard or the services page, and select \"Finish\".</p></td>
                            </tbody>
                        </table>
                        <table class='table table-striped' style='text-align: left;'>
                            <thead>
                                <th class='styled'>Invoices</th>
                            </thead>
                            <tbody>
                                <td><p>Invoices can be generated by clicking the \"Generate Invoice\" button on the service of your choice. They are dynamically generated and can be sent with the link to the page, or printed off, whichever is the better option for you. To print the page, simply click the options button in the top right corner of your browser of choice. One of the options will be print, you can even turn them into PDFs this way if you prefer.</p></td>
                            </tbody>
                        </table>
                        <table class='table table-striped' style='text-align: left;'>
                            <thead>
                                <th class='styled'>Administration</th>
                            </thead>
                            <tbody>
                                <td><p>The administration is to allow you to create other users for the panel and keep track of who has access to the panel, as well as to change your username and password if you so desire. There are two levels of user in the panel, though they only differ very slightly, Administrators simply have a more robust Administration section, being able to add, view and manage other users on the panel, where as ordinary users cannot add remove or see other users, only change their own username and password.</p></td>
                            </tbody>
                        </table>
                        <table class='table table-striped' style='text-align: left;'>
                            <thead>
                                <th class='styled'>Searches</th>
                            </thead>
                            <tbody>
                                <td><p>The Search bar is universal, it can be used to search for nearly anything from anywhere on the panel. You can search for customers and cars by customer name, or by car registration, these are interchangeable when searching. And you can search for service by service ID, Registration Number or by Customer.</br></br>
Remember: When searching by customer name, you can search by first name, last name or both, depending on how specific you want to be, this is automatic, and you do not need to worry about making sure you spell everything perfectly, the system has some leaway.</p></td>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>";
    return $return;
}