<?php
include_once 'internal_data/_settings.php';
include_once 'internal_data/_connect.php';
include_once 'internal_data/functions/coreFunctions.php';

$servicesTable = $_SETTINGS['SERVICES_TABLE'];

if (isset($_SESSION['Username'])) {
    if(isset($_POST['finishButton'])){
        $serviceID = $_POST['serviceID'];
        $command = "UPDATE $servicesTable SET Service_Finished = NOW()"
                . " WHERE Service_ID = $serviceID";
        
        $query = mysqli_query($connection, $command) or die(mysqli_error());
    }
} else {
    header("Location: index.php");
}
?>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link rel="stylesheet" type="text/css" href="design/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="design/css/style.css">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="design/css/js/bootstrap.min.js"></script>

        <title>BARSC Dashboard</title>
    </head>
    <body>
        <?php
        print returnSidebar("overview");
        ?>
        <div class="content-container">
            <div class="main-content" style="text-align: center;">
                <h1 class="content-title">Ongoing Services</h1>
                <?php
                print ongoingServicesTable();
                ?>
            </div>
        </div>

        <div class="modal fade" id="logout" tabindex="-1" role="dialog" aria-labelledby="logout" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <p class="modal-title">Are you sure you want to logout?</p>
                    </div>
                    <div class="modal-body">
                        <div style="text-align: center;">
                            <form method="post" action="internal_data/functions/logout.php">
                                <button class="btn btn-primary" type="submit" name="logout">Logout</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </body>
    <footer>



    </footer>
</html>