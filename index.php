<?php
include_once('internal_data/_settings.php');
include_once('internal_data/_connect.php');

$usersTable = $_SETTINGS['USERS_TABLE'];

$errorMessage = null;

if (isset($_POST['login'])) {

    $username = $_POST['username'];
    $password = hash("sha256", $_POST['password']);

    $username = stripslashes($username);
    $username = $connection->real_escape_string($username);

    $password = stripslashes($password);
    $password = $connection->real_escape_string($password);

    $command = "SELECT * FROM $usersTable WHERE Username = '$username' AND Password = '$password'";
    $query = mysqli_query($connection, $command) or die(mysqli_error());
    $result = mysqli_fetch_row($query);

    $userExists = mysqli_num_rows($query) == 1 ? true : false;

    if ($userExists) {
        $_SESSION['Username'] = $username;
        $_SESSION['Rank'] = $result[3];
        header("Location: overview.php");
    } else {
        $errorMessage = "Incorrect Username or Password!";
    }
}
?>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" type="text/css" href="design/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="design/css/style.css">

        <title>BARSC Login</title>
    </head>
    <body>
        <div class="logo-container"><h1 class="logo">BARSC</h1></div>
        <div class="login-container">
            <div class="login-content" style="text-align: center;">
                <h2>Login</h2>
                <?php
                if ($errorMessage != null) {
                    print "<span class='label label-warning'>$errorMessage</span>";
                }
                ?>
                <form class = 'form-signin' role = 'form' action = '' method = 'post' autocomplete="off">
                    <input class="login-input" type = 'text' name = 'username' placeholder = 'Username' required>
                    <input class="login-input" type = 'password' name = 'password' placeholder = 'Password' required>
                    <button class='btn btn-primary' style='width: 60px;' name='login' type='submit'>Login</button>
                </form>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>
