<?php
include_once 'internal_data/_settings.php';
include_once 'internal_data/_connect.php';
include_once 'internal_data/functions/coreFunctions.php';

if (isset($_SESSION['Username'])) {

    $servicesTable = $_SETTINGS['SERVICES_TABLE'];
    $customersTable = $_SETTINGS['CUSTOMERS_TABLE'];
    $carsTable = $_SETTINGS['CARS_TABLE'];

    $servicesRangeLow = 0;
    $servicesRangeHigh = 20;

    $command = "SELECT $carsTable.*, $customersTable.* FROM $carsTable"
            . " JOIN $customersTable ON $carsTable.Customer_ID = $customersTable.Customer_ID"
            . " ORDER BY $carsTable.Car_ID DESC LIMIT $servicesRangeLow, $servicesRangeHigh;";

    if (isset($_POST['searchValue'])) {
        $search = $_POST['searchValue'];

        $command = "SELECT $carsTable.*, $customersTable.* FROM $carsTable"
                . " LEFT JOIN $customersTable ON $carsTable.Customer_ID = $customersTable.Customer_ID"
                . " WHERE $customersTable.Name LIKE '%$search%' OR $carsTable.Registration_No = '$search'"
                . " ORDER BY $carsTable.Car_ID;";
    } else if (isset($_POST['searchCustName'])) {
        $search = $_POST['searchCustName'];

        $command = "SELECT * FROM $customersTable WHERE Name LIKE '%$search%';";
    } else if(isset($_POST['addCarSubmit'])){
        $customerID = $_POST['customerID'];
        $make = $_POST['make'];
        $model = $_POST['model'];
        $registration = $_POST['reg'];
        $colour = $_POST['colour'];
        
        $command = "INSERT INTO $carsTable (Customer_ID, Registration_No, Make, Model, Colour) VALUES ('$customerID', '$registration', '$make', '$model', '$colour');";
        $query = mysqli_query($connection, $command) or die(mysqli_error());
        
        $command = "SELECT $carsTable.*, $customersTable.* FROM $carsTable"
            . " JOIN $customersTable ON $carsTable.Customer_ID = $customersTable.Customer_ID"
            . " ORDER BY $carsTable.Car_ID DESC LIMIT $servicesRangeLow, $servicesRangeHigh;";
    } else if (isset($_POST['editCustName'])) {
        $search = $_POST['editCustName'];
        $carID = $_POST['carID'];

        $command = "SELECT * FROM $customersTable WHERE Name LIKE '%$search%';";
    } else if(isset($_POST['editCarSubmit'])){
        $carID = $_POST['carID'];
        $customerID = $_POST['customerID'];
        
        $command = "UPDATE $carsTable SET Customer_ID='$customerID' WHERE Car_ID='$carID';";
        $query = mysqli_query($connection, $command) or die(mysqli_error());
        
        $command = "SELECT $carsTable.*, $customersTable.* FROM $carsTable"
                . " LEFT JOIN $customersTable ON $carsTable.Customer_ID = $customersTable.Customer_ID"
                . " WHERE $carsTable.Car_ID = '$carID'"
                . " ORDER BY $carsTable.Car_ID;";
    } else if(isset($_POST['DeleteCar'])){
        $carID = $_POST['carID'];
        
        $command = "DELETE FROM $carsTable WHERE Car_ID = '$carID';";
        $query = mysqli_query($connection, $command) or die(mysqli_error());
        
        $command = "SELECT $carsTable.*, $customersTable.* FROM $carsTable"
            . " JOIN $customersTable ON $carsTable.Customer_ID = $customersTable.Customer_ID"
            . " ORDER BY $carsTable.Car_ID DESC LIMIT $servicesRangeLow, $servicesRangeHigh;";
    } else if(isset($_POST['editCarDetailsSubmit'])){
        $make = $_POST['make'];
        $model = $_POST['model'];
        $registration = $_POST['reg'];
        $colour = $_POST['colour'];
        $carID = $_POST['carID'];
        
        $command = "UPDATE $carsTable SET Make='$make', Model='$model', Registration_No='$registration', Colour='$colour' WHERE Car_ID='$carID';";
        $query = mysqli_query($connection, $command) or die(mysqli_error());
        
        $command = "SELECT $carsTable.*, $customersTable.* FROM $carsTable"
                . " LEFT JOIN $customersTable ON $carsTable.Customer_ID = $customersTable.Customer_ID"
                . " WHERE $carsTable.Car_ID = '$carID'"
                . " ORDER BY $carsTable.Car_ID;";
    }

    $query = mysqli_query($connection, $command) or die(mysqli_error());
} else {
    header("Location: index.php");
}
?>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" type="text/css" href="design/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="design/css/style.css">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="design/css/js/bootstrap.min.js"></script>

        <title>BARSC Cars</title>
    </head>
    <body>
        <?php
        print returnSidebar("cars");
        ?>
        <div class="content-container">
            <div class="main-content" style="text-align: center;">
                <h1 class="content-title">Cars</h1>
                <center>
                    <?php
                    while ($return = mysqli_fetch_array($query)) {
                        $servicesCommand = "SELECT * FROM $servicesTable WHERE Car_ID = '$return[0]';";
                        $servicesQuery = mysqli_query($connection, $servicesCommand) or die(mysqli_error());
                        if (isset($_POST['searchCustName'])) {
                            print "<div class='inner-content-block'>
                            <h2 class='inner-content-title'>$return[1]</h2>
                                <p class='inner-content-text'><strong>DOB: </strong>$return[3]</p>
                                <p class='inner-content-text'><strong>Address: </strong>$return[2]</p>
                                <p style='position: absolute; top: 11px; right: 10px; color: white;'>ID: $return[0]</p>
                                <button class='btn btn-primary' style='width: 240px; position: absolute; bottom: -5px; left: 50%; transform: translateX(-50%);' data-toggle='modal' href='#select$return[0]'>Select</button></div>";
                            
                            print "<div class='modal fade' id='select$return[0]' tabindex='-1' role='dialog' aria-labelledby='select$return[0]' aria-hidden='true'>
                                        <div class='modal-dialog modal-sm'>
                                            <div class='modal-content'>
                                                <div class='modal-header'>
                                                    <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                                                    <p class='modal-title'>Add Car</p>
                                                </div>
                                                <div class='modal-body'>
                                                    <form class='form-signin' name='addCustomerForm' method='post' action=''>
                                                        <input class='form-control form-styling' type='hidden' name='customerID' value='$return[0]' required>
                                                        <input class='form-control form-styling' type='text' name='reg' placeholder='Registration Number' required>
                                                        <input class='form-control form-styling' type='text' name='make' placeholder='Make' required>
                                                        <input class='form-control form-styling' type='text' name='model' placeholder='Model' required>
                                                        <input class='form-control form-styling' type='text' name='colour' placeholder='Colour' required>
                                                        
                                                        <div class='modal fade' id='confirmAdd$return[0]' tabindex='-1' role='dialog' aria-labelledby='confirmAdd$return[0]' aria-hidden='true'>
                                                            <div class='modal-dialog modal-sm'>
                                                                <div class='modal-content'>
                                                                    <div class='modal-header'>
                                                                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                                                                        <p class='modal-title'>Add Car To Customer?</p>
                                                                    </div>
                                                                    <div class='modal-body'>
                                                                        <form class='form-signin' name='addCustomerForm' method='post' action=''>
                                                                            <button style='margin-top: 10px;' class='btn btn-primary' type='submit' name='addCarSubmit'>Add</button>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <button class='btn btn-primary' data-toggle='modal' href='#confirmAdd$return[0]'>Select</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>";
                        } else if (isset($_POST['editCustName'])) {
                            print "<div class='inner-content-block'>
                            <h2 class='inner-content-title'>$return[1]</h2>
                                <p class='inner-content-text'><strong>DOB: </strong>$return[2]</p>
                                <p class='inner-content-text'><strong>Phone Number: </strong>$return[3]</p>
                                <p style='position: absolute; top: 11px; right: 10px; color: white;'>ID: $return[0]</p>
                                <button class='btn btn-primary' style='width: 240px; position: absolute; bottom: -5px; left: 50%; transform: translateX(-50%);' data-toggle='modal' href='#confirmAdd$return[0]'>Select</button></div>";
                            
                            print "<div class='modal fade' id='confirmAdd$return[0]' tabindex='-1' role='dialog' aria-labelledby='confirmAdd$return[0]' aria-hidden='true'>
                                                            <div class='modal-dialog modal-sm'>
                                                                <div class='modal-content'>
                                                                    <div class='modal-header'>
                                                                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                                                                        <p class='modal-title'>Add Car To Customer?</p>
                                                                    </div>
                                                                    <div class='modal-body'>
                                                                        <form class='form-signin' name='editCarForm' method='post' action=''>
                                                                            <input type='hidden' name='customerID' value='$return[0]'>
                                                                            <input type='hidden' name='carID' value='$carID'>
                                                                            <button style='margin-top: 10px;' class='btn btn-primary' type='submit' name='editCarSubmit'>Edit</button>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>";
                        } else {
                            print "<div class='inner-content-block'>
                            <h2 class='inner-content-title' style='text-transform: uppercase;'>$return[2]</h1>
                                <p class='inner-content-text'><strong>Make: </strong>$return[3]</p>
                                <p class='inner-content-text'><strong>Model: </strong>$return[4]</p>
                                <p class='inner-content-text'><strong>Owner: </strong>$return[7]</p>
                                <p style='position: absolute; top: 11px; right: 10px; color: white;'>ID: $return[0]</p>
                                <button class='btn btn-primary' style='width: 240px; position: absolute; bottom: -5px; left: 50%; transform: translateX(-50%);' data-toggle='modal' href='#sid$return[0]'>More Info</button>
                            </div>";
                            
                            print " <div class='modal fade' id='sid$return[0]' tabindex='-1' role='dialog' aria-labelledby='moreInfo' aria-hidden='true' >
                                    <div class='modal-dialog modal-sm' style='width: 800px;'>
                                        <div class='modal-content'>
                                            <div class='modal-header'>
                                                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                                                <p class='modal-title'>Car $return[0] More Information</p>
                                            </div>
                                            <div class='modal-body' >
                                                <table class='table table-striped'>
                                        <thead>
                                            <tr>
                                                <th colspan='5' class='styled'>Owner</th>
                                            </tr><tr>
                                                <th class='styled'>ID</th>
                                                <th class='styled'>Name</th>
                                                <th class='styled'>Date of Birth</th>
                                                <th class='styled'>Address</th>
                                                <th class='styled'>Phone Number</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>$return[6]</td>
                                                <td>$return[7]</td>
                                                <td>$return[8]</td>
                                                <td>$return[10],</br>". ($return[11] != null ? "$return[11],</br>" : "") ."$return[13],</br> $return[14],</br> $return[12]</td>
                                                <td>$return[9]</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class='table table-striped'>
                                        <thead>
                                            <tr>
                                                <th colspan='6' class='styled'>Information</th>
                                            </tr><tr>
                                                <th class='styled'>ID</th>
                                                <th class='styled'>Registration</th>
                                                <th class='styled'>Make</th>
                                                <th class='styled'>Model</th>
                                                <th class='styled'>Colour</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                        <tr>
                                       <td>$return[0]</td>"
                                    . "<td style='text-transform: uppercase;'>$return[2]</td>"
                                    . "<td>$return[3]</td>"
                                    . "<td>$return[4]</td>"
                                    . "<td>$return[5]</td>"
                                    . "</tr>
                                        </tbody>
                                    </table>
                                    <table class='table table-striped' style='margin-bottom: 40px;'>
                                        <thead>
                                            <tr>
                                                <th colspan='4' class='styled'>Services</th>
                                            </tr><tr>
                                                <th class='styled'>ID</th>
                                                <th class='styled'>Type</th>
                                                <th class='styled'>Started</th>
                                                <th class='styled'>-</th>
                                            </tr>
                                        </thead>
                                        <tbody>";
                                    while($services = mysqli_fetch_array($servicesQuery)){
                                        $type = $services[3] == "Other" ? "$services[3]: $services[4]" : $services[3];
                                        print "<tr>"
                                            . "<td>$services[0]</td>"
                                            . "<td>$type</td>"
                                            . "<td>$services[5]</td>"
                                            . "<td><form action='services.php' method='post'>"
                                            . "<input type='hidden' name='searchValue' value='$services[0]'>"
                                            . "<button class='btn btn-primary' type='submit' name='submitButton'>Go To</button>"
                                            . "</form></td></tr>";
                                    }
                                        print "</tbody>
                                    </table>
                                            <button class='btn btn-primary' style='position: absolute; width: 120px; bottom: 5px; left: 5px;' data-toggle='modal' href='#editCar$return[0]'>Edit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>";
                                        
print "<div class='modal fade' id='editCar$return[0]' tabindex='-1' role='dialog' aria-labelledby='editService$return[0]' aria-hidden='true'>

            <div class='modal-dialog modal-sm'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                        <p class='modal-title'>Edit Car:</p>
                    </div>
                    <div class='modal-body' style='text-align: left;'>
                        <button class='btn btn-primary' style='margin: 10px 0px; width: 270px;' data-toggle='modal' href='#editOwner$return[0]'>Change Owner</button>
                        <button class='btn btn-primary' style='margin: 10px 0px; width: 270px;' data-toggle='modal' href='#editDetails$return[0]'>Change Information</button>
                        <button class='btn btn-primary' style='margin: 10px 0px; width: 270px;' data-toggle='modal' href='#delete$return[0]'>Delete Car</button>
                    </div>
                </div>
            </div>
        </div>";

print "<div class='modal fade' id='editDetails$return[0]' tabindex='-1' role='dialog' aria-labelledby='editDetails$return[0]' aria-hidden='true'>
            <div class='modal-dialog modal-sm'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                        <p class='modal-title'>Edit Car</p>
                    </div>
                    <div class='modal-body'>
                        <form class='form-signin' name='addCustomerForm' method='post' action=''>
                            <input class='form-control form-styling' type='hidden' name='carID' value='$return[0]' required>
                            <input class='form-control form-styling' type='text' name='reg' value='$return[2]' placeholder='Registration Number' required>
                            <input class='form-control form-styling' type='text' name='make' value='$return[3]' placeholder='Make' required>
                            <input class='form-control form-styling' type='text' name='model' value='$return[4]' placeholder='Model' required>
                            <input class='form-control form-styling' type='text' name='colour' value='$return[5]' placeholder='Colour' required>
                            <button style='margin-top: 10px;' class='btn btn-primary' type='submit' name='editCarDetailsSubmit'>Edit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>";

print "<div class='modal fade' id='delete$return[0]' tabindex='-1' role='dialog' aria-labelledby='delete$return[0]' aria-hidden='true'>

            <div class='modal-dialog modal-sm'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                        <p class='modal-title'>Are you sure?</p>
                    </div>
                    <div class='modal-body' style='text-align: left;'>
                        <form action='' method='post' name='deleteForm'>
                            <input type='hidden' name='carID' value='$return[0]'/>
                            <button class='btn btn-primary' style='margin: 10px 0px; width: 270px;' type='submit' name='DeleteCar'>Delete Car?</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>";

print "<div class='modal fade' id='editOwner$return[0]' tabindex='-1' role='dialog' aria-labelledby='editOwner$return[0]' aria-hidden='true'>
            <div class='modal-dialog modal-sm'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                        <p class='modal-title'>Search For Customer:</p>
                    </div>
                    <div class='modal-body'>
                        <form class='form-signin' name='editCustomerForm' method='post' action=''>
                            <input class='login-input' type='text' name='editCustName' required>
                            <input type='hidden' name='carID' value='$return[0]'>
                            <button style='margin-top: -2px;' class='btn btn-primary' type='submit'>Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>";
                        }
                    }
                    ?>
                </center>

                <?php
                if ($servicesRangeLow > 0) {
                    print "<button class='btn btn-primary' style='position: absolute; width: 120px; bottom: 5px; left: 5px;' data-toggle='modal' href=''>Previous Page</button>";
                } else {
                    print "<button class='btn btn-primary' style='position: absolute; width: 120px; bottom: 5px; left: 5px;' data-toggle='modal' href='' disabled>Previous Page</button>";
                }

                if ($rowCount > $servicesRangeHigh) {
                    print "<button class='btn btn-primary' style='position: absolute; width: 120px; bottom: 5px; left: 130px;' data-toggle='modal' href=''>Next Page</button>";
                } else {
                    print "<button class='btn btn-primary' style='position: absolute; width: 120px; bottom: 5px; left: 130px;' data-toggle='modal' href='' disabled>Next Page</button>";
                }
                ?>

                <button class='btn btn-primary' style='position: absolute; width: 170px; bottom: 5px; right: 5px;' data-toggle='modal' href='#searchCustomer'>New Car</button>

            </div>
        </div>

        <div class='modal fade' id='searchCustomer' tabindex='-1' role='dialog' aria-labelledby='searchCustomer' aria-hidden='true'>
            <div class='modal-dialog modal-sm'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                        <p class='modal-title'>Search For Customer:</p>
                    </div>
                    <div class='modal-body'>
                        <form class='form-signin' name='searchCustomerForm' method='post' action=''>
                            <input class='login-input' type='text' name='searchCustName' required>
                            <button style='margin-top: -2px;' class='btn btn-primary' type='submit'>Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="logout" tabindex="-1" role="dialog" aria-labelledby="logout" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <p class="modal-title">Are you sure you want to logout?</p>
                    </div>
                    <div class="modal-body">
                        <div style="text-align: center;">
                            <form method="post" action="internal_data/functions/logout.php">
                                <button class="btn btn-primary" type="submit" name="logout">Logout</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </body>
    <footer>



    </footer>
</html>