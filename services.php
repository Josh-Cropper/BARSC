<?php
include_once('internal_data/_settings.php');
include_once('internal_data/_connect.php');
include_once 'internal_data/functions/coreFunctions.php';
include_once 'internal_data/functions/Data.php';

if (isset($_SESSION['Username'])) {

    $servicesTable = $_SETTINGS['SERVICES_TABLE'];
    $customersTable = $_SETTINGS['CUSTOMERS_TABLE'];
    $carsTable = $_SETTINGS['CARS_TABLE'];
    $partsTable = $_SETTINGS['PARTS_TABLE'];

    $servicesRangeLow = 0;
    $servicesRangeHigh = 20;

    $rowCount = 0;
    $page = 1;
    
    $errorMessage = null;

    if (isset($_GET['page'])) {
        $page = $_GET['page'];
        $servicesRangeLow = ($page * 20) - 20;
        $servicesRangeHigh = $page * 20;
    }

    $command = "SELECT $servicesTable.*, $customersTable.*, $carsTable.* FROM $servicesTable JOIN $customersTable ON "
            . "$servicesTable.Customer_ID = $customersTable.Customer_ID JOIN $carsTable ON $servicesTable.Car_ID = $carsTable.Car_ID ORDER BY $servicesTable.Service_ID desc LIMIT $servicesRangeLow, $servicesRangeHigh;";

    if (isset($_POST['searchValue'])) {
        $search = $_POST['searchValue'];

        $command = "SELECT $servicesTable.*, $customersTable.*, $carsTable.* FROM $servicesTable JOIN $customersTable ON "
                . "$servicesTable.Customer_ID = $customersTable.Customer_ID JOIN $carsTable ON $servicesTable.Car_ID = $carsTable.Car_ID"
                . " WHERE $servicesTable.Service_ID = '$search' OR $customersTable.Name LIKE '%$search%' OR $carsTable.Registration_No = '$search';";
    } else if (isset($_POST['searchServicesCustName'])) {
        $search = $_POST['searchServicesCustName'];

        $command = "SELECT $carsTable.*, $customersTable.* FROM $carsTable JOIN $customersTable ON "
                . "$carsTable.Customer_ID = $customersTable.Customer_ID"
                . " WHERE $customersTable.Name LIKE '%$search%';";
    } else if (isset($_POST['searchServicesReg'])) {
        $search = $_POST['searchServicesReg'];

        $command = "SELECT $carsTable.*, $customersTable.* FROM $carsTable JOIN $customersTable ON "
                . "$carsTable.Customer_ID = $customersTable.Customer_ID"
                . " WHERE Registration_No LIKE '%$search%';";
    } else if (isset($_POST['ServiceAdd'])) {
        $carID = $_POST['carID'];
        $customerID = $_POST['customerID'];
        $serviceType = $_POST['serviceType'];

        if ($serviceType == 'Other') {
            $other = $_POST['OtherInput'];

            $command = "INSERT INTO $servicesTable (Car_ID, Customer_ID, Service_Type, Other) VALUES ('$carID', '$customerID', 'Other', '$other');";
        } else {
            $command = "INSERT INTO $servicesTable (Car_ID, Customer_ID, Service_Type) VALUES ('$carID', '$customerID', '$serviceType');";
        }
        $query = mysqli_query($connection, $command) or die(mysqli_error());

        $command = "SELECT $servicesTable.*, $customersTable.*, $carsTable.* FROM $servicesTable JOIN $customersTable ON "
                . "$servicesTable.Customer_ID = $customersTable.Customer_ID JOIN $carsTable ON $servicesTable.Car_ID = $carsTable.Car_ID ORDER BY $servicesTable.Service_ID desc LIMIT $servicesRangeLow, $servicesRangeHigh;";
    } else if(isset($_POST['editServiceType'])) {
        $type = $_POST['editServiceType'];
        $other = $_POST['OtherInput'];
        $search = $_POST['serviceID'];
        $updateCommand = ($type == "Other") ? "UPDATE $servicesTable SET Service_Type = '$type', Other = '$other' WHERE Service_ID = '$search';" : "UPDATE $servicesTable SET Service_Type = '$type' WHERE Service_ID = '$search';";
        
        $errorMessage = "Service $search Updated Succesfully!";
        
        $updateQuery = mysqli_query($connection, $updateCommand) or die($errorMessage = "Oops, Something went wrong!");
        
        $command = "SELECT $servicesTable.*, $customersTable.*, $carsTable.* FROM $servicesTable JOIN $customersTable ON "
                . "$servicesTable.Customer_ID = $customersTable.Customer_ID JOIN $carsTable ON $servicesTable.Car_ID = $carsTable.Car_ID"
                . " WHERE $servicesTable.Service_ID = '$search';";
    } else if(isset($_POST['DeleteService'])){
        $serviceID = $_POST['serviceID'];
        
        $updateCommand = "DELETE FROM $servicesTable WHERE Service_ID = '$serviceID';";
        
        $errorMessage = "Service $serviceID Deleted Succesfully!";
        
        $updateQuery = mysqli_query($connection, $updateCommand) or die($errorMessage = "Oops, Something went wrong!");
        
        $command = "SELECT $servicesTable.*, $customersTable.*, $carsTable.* FROM $servicesTable JOIN $customersTable ON "
                . "$servicesTable.Customer_ID = $customersTable.Customer_ID JOIN $carsTable ON $servicesTable.Car_ID = $carsTable.Car_ID;";
    } else if (isset($_POST['editServicesCustName'])) {
        $search = $_POST['editServicesCustName'];
        $serviceID = $_POST['serviceID'];

        $command = "SELECT $carsTable.*, $customersTable.* FROM $carsTable JOIN $customersTable ON "
                . "$carsTable.Customer_ID = $customersTable.Customer_ID"
                . " WHERE $customersTable.Name LIKE '%$search%';";
    } else if (isset($_POST['editServicesReg'])) {
        $search = $_POST['editServicesReg'];
        $serviceID = $_POST['serviceID'];

        $command = "SELECT $carsTable.*, $customersTable.* FROM $carsTable JOIN $customersTable ON "
                . "$carsTable.Customer_ID = $customersTable.Customer_ID"
                . " WHERE Registration_No LIKE '%$search%';";
    } else if(isset($_POST['changeCar'])){
        $serviceID = $_POST['serviceID'];
        $carID = $_POST['changeCar'];
        $customerID = $_POST['customerID'];
        
        $updateCommand = "UPDATE $servicesTable SET Car_ID = '$carID', Customer_ID = '$customerID' WHERE Service_ID = '$serviceID';";
        
        $errorMessage = "Service $serviceID Updated Succesfully!";
        
        $updateQuery = mysqli_query($connection, $updateCommand) or die($errorMessage = "Oops, Something went wrong!");
        
        $command = "SELECT $servicesTable.*, $customersTable.*, $carsTable.* FROM $servicesTable JOIN $customersTable ON "
                . "$servicesTable.Customer_ID = $customersTable.Customer_ID JOIN $carsTable ON $servicesTable.Car_ID = $carsTable.Car_ID"
                . " WHERE $servicesTable.Service_ID = '$serviceID';";
    } else if(isset($_POST['addPart'])){
        $serviceID = $_POST['serviceID'];
        $partName = $_POST['name'];
        $partPrice = $_POST['price'];
        
        $command = "INSERT INTO $partsTable (Part_Name, Part_Price, Service_ID) VALUES ('$partName', '$partPrice', '$serviceID');";
        $query = mysqli_query($connection, $command) or die(mysqli_error());
        
        $command = "SELECT $servicesTable.*, $customersTable.*, $carsTable.* FROM $servicesTable JOIN $customersTable ON "
                . "$servicesTable.Customer_ID = $customersTable.Customer_ID JOIN $carsTable ON $servicesTable.Car_ID = $carsTable.Car_ID"
                . " WHERE $servicesTable.Service_ID = '$serviceID';";
    } else if(isset($_POST['DeletePart'])) {
        $serviceID = $_POST['serviceID'];
        $partID = $_POST['partID'];
        
        $command = "DELETE FROM $partsTable WHERE Part_ID='$partID';";
        
        $query = mysqli_query($connection, $command) or die(mysqli_error());
        
        $command = "SELECT $servicesTable.*, $customersTable.*, $carsTable.* FROM $servicesTable JOIN $customersTable ON "
                . "$servicesTable.Customer_ID = $customersTable.Customer_ID JOIN $carsTable ON $servicesTable.Car_ID = $carsTable.Car_ID"
                . " WHERE $servicesTable.Service_ID = '$serviceID';";
    }

    $query = mysqli_query($connection, $command) or die(mysqli_error());

    $rowCount = mysqli_num_rows($query);
} else {
    header("Location: index.php");
}
?>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" type="text/css" href="design/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="design/css/style.css">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="design/css/js/bootstrap.min.js"></script>

        <title>BARSC Services</title>
    </head>
    <body>
<?php
print returnSidebar("services");
?>
        <div class="content-container">
            <div class="main-content" style="text-align: center; overflow: hidden;">
                <h1 class="content-title"><?php (isset($_POST['searchServicesCustName']) || isset($_POST['searchServicesReg'])) ? print "Select Car for New Service" : print "Services" ?></br>
                <?php if ($errorMessage != null) {
                    $errorMessage == "Oops, Something went wrong!" ? print "<span class='label label-warning' style='position: relative; top: 7px;'>$errorMessage</span>"
                                     : print "<span class='label label-success' style='position: relative; top: 7px;'>$errorMessage</span>";
                } ?></h1>
                <center>
<?php

while ($return = mysqli_fetch_array($query)) {
    $serviceID = $return[0];
    $partsCommand = "SELECT * FROM $partsTable WHERE Service_ID = $return[0]";
    $partsQuery = mysqli_query($connection, $partsCommand) or die(mysqli_error());
    
    $otherDetails = $return[3] == "Other" ? "$return[3]: $return[4]" : $return[3] ;
    $finishDate = $return[6] == null ? "<form name='finishService' method='post' action=''>
                                                        <input type='hidden' name='serviceID' value='$return[0]'>
                                                        <button class='btn btn-primary' type='submit' name='finishButton'>Finish</button>
                                                    </form>" :
        "$return[6]";
    $finishDate = $return[7] == null ? $finishDate : $return[7];
    $finishedTitle = $return[7] == null ? "Finished" : "Aborted";
    
    if ((isset($_POST['searchServicesCustName']) || isset($_POST['searchServicesReg']))) {

        print "<div class='inner-content-block'>
                            <h2 class='inner-content-title' style='text-transform: uppercase;'>$return[2]</h1>
                                <p class='inner-content-text'><strong>Make: </strong>$return[3]</p>
                                <p class='inner-content-text'><strong>Model: </strong>$return[4]</p>
                                <p class='inner-content-text'><strong>Owner: </strong>$return[7]</p>
                                <p style='position: absolute; top: 11px; right: 10px; color: white;'>ID: $return[0]</p>
                                <button class='btn btn-primary' style='width: 240px; position: absolute; bottom: -5px; left: 50%; transform: translateX(-50%);' data-toggle='modal' href='#createServ$return[0]'>Select</button>
                            </div>";

        print " <div class='modal fade' id='createServ$return[0]' tabindex='-1' role='dialog' aria-labelledby='createServ' aria-hidden='true' >

                                    <script>
                                        $(document).ready(function () {
                                            toggleFields();
                                            $('#serviceType').change(function () {
                                                toggleFields();
                                            });
                                        });
                                        
                                        function toggleFields() {
                                            if ($('#serviceType').val() == 'Other')
                                                $('#other$return[0]').show();
                                            else
                                                $('#other$return[0]').hide();
                                            }
                                    </script>

                                    <div class='modal-dialog modal-sm' style='width: 400px;'>
                                        <div class='modal-content'>
                                            <div class='modal-header'>
                                                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                                                <p class='modal-title'>Create Service For Car: $return[0]</p>
                                            </div>
                                            <div class='modal-body' >
                                                <form class='form-signin' name='createServiceForm' action='' method='post'>
                                                    <input class='login-input form-styling' type='hidden' name='carID' value='$return[0]' readonly>
                                                    <input class='login-input form-styling' type='hidden' name='customerID' value='$return[1]' readonly>
                                                    <select class='form-control form-styling' id='serviceType' name='serviceType' required>
                                                        <option value='MOT'>MOT</option>
                                                        <option value='Tire Change'>Tyre Change</option>
                                                        <option value='Internal Electronics'>Internal Electronics</option>
                                                        <option value='Engine Repair'>Engine Repair</option>
                                                        <option value='Other'>Other</option>
                                                    </select>
                                                    <div id='other$return[0]'>
                                                        <input class='form-control form-styling' type='text' name='OtherInput' placeholder='Other'>
                                                    </div>
                                                    <button class='btn btn-primary' type='submit' name='ServiceAdd'>Add Service</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>";
    } else if((isset($_POST['editServicesCustName']) || isset($_POST['editServicesReg']))){
        $customerID = $return[1];
        print "<div class='inner-content-block'>
                            <h2 class='inner-content-title' style='text-transform: uppercase;'>$return[2]</h1>
                                <p class='inner-content-text'><strong>Make: </strong>$return[3]</p>
                                <p class='inner-content-text'><strong>Model: </strong>$return[4]</p>
                                <p class='inner-content-text'><strong>Owner: </strong>$return[7]</p>
                                <p style='position: absolute; top: 11px; right: 10px; color: white;'>ID: $return[0]</p>
                                <button class='btn btn-primary' style='width: 240px; position: absolute; bottom: -5px; left: 50%; transform: translateX(-50%);' data-toggle='modal' href='#editServ$return[0]'>Select</button>
                            </div>";

        print " <div class='modal fade' id='editServ$return[0]' tabindex='-1' role='dialog' aria-labelledby='createServ' aria-hidden='true' >

                                    <div class='modal-dialog modal-sm' style='width: 400px;'>
                                        <div class='modal-content'>
                                            <div class='modal-header'>
                                                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                                                <p class='modal-title'>Edit Service $serviceID To Car $return[0]?</p>
                                            </div>
                                            <div class='modal-body' >
                                                <form class='form-signin' name='editServiceForm' action='' method='post'>
                                                    <input class='login-input form-styling' type='hidden' name='changeCar' value='$return[0]' readonly>
                                                    <input class='login-input form-styling' type='hidden' name='customerID' value='$customerID' readonly>
                                                    <input class='login-input form-styling' type='hidden' name='serviceID' value='$serviceID' readonly>
                                                    <button class='btn btn-primary' style='margin: 10px 0px; width: 270px;' type='submit' name='ServiceEdit'>Select Car?</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>";
    } else {
        print "<div class='inner-content-block'>
                            <h2 class='inner-content-title'>Service ID: " . $return[0] . "</h1>
                                <p class='inner-content-text'><strong>Name: </strong>$return[9]</p>
                                <p class='inner-content-text' style='margin-left: -115px; display: inline;'><strong>Car Registration: </strong></p><p class='inner-content-text' style='text-transform: uppercase; display: inline; margin-left: -3px;'>$return[19]</p>
                                <p class='inner-content-text'><strong>Service Type: </strong>$return[3]</p>
                                <p style='position: absolute; top: 11px; right: 10px; color: white;'>$return[5]</p>
                                <button class='btn btn-primary' style='width: 170px; position: absolute; bottom: -5px; left: -5px;' data-toggle='modal' href='#sid$return[0]'>More Info</button>
                                <button class='btn btn-primary' style='width: 170px; position: absolute; bottom: -5px; right: -5px;' onClick='window.open(\"/internal_data/functions/Invoice.php?serviceID=$return[0]\");'>Generate Invoice</button>
                            </div>";

        print " <div class='modal fade' id='sid$return[0]' tabindex='-1' role='dialog' aria-labelledby='moreInfo' aria-hidden='true' >
                                    <div class='modal-dialog modal-sm' style='width: 800px;'>
                                        <div class='modal-content'>
                                            <div class='modal-header'>
                                                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                                                <p class='modal-title'>Service $return[0] More Information</p>
                                            </div>
                                            <div class='modal-body' >
                                                <table class='table table-striped'>
                                        <thead>
                                            <tr>
                                                <th colspan='6' class='styled'>Customer</th>
                                            </tr><tr>
                                                <th class='styled'>ID</th>
                                                <th class='styled'>Name</th>
                                                <th class='styled'>Date of Birth</th>
                                                <th class='styled'>Address</th>
                                                <th class='styled'>Phone Number</th>
                                                <th class='styled'>Cars</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>$return[8]</td>
                                                <td>$return[9]</td>
                                                <td>$return[10]</td>
                                                <td>$return[12],</br>". ($return[13] != null ? "$return[13],</br>" : "") ."$return[15],</br> $return[16],</br> $return[14]</td>
                                                <td>$return[11]</td>
                                                <td><form action='cars.php' method='post'>"
                . "<input type='hidden' name='searchValue' value='$return[9]'>"
                . "<button class='btn btn-primary' type='submit' name='submitButton'>Find</button>"
                . "</form></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class='table table-striped'>
                                        <thead>
                                            <tr>
                                                <th colspan='5' class='styled'>Car</th>
                                            </tr><tr>
                                                <th class='styled'>ID</th>
                                                <th class='styled'>Registration</th>
                                                <th class='styled'>Make</th>
                                                <th class='styled'>Model</th>
                                                <th class='styled'>Colour</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>$return[17]</td>
                                                <td style='text-transform: uppercase;'>$return[19]</td>
                                                <td>$return[20]</td>
                                                <td>$return[21]</td>
                                                <td>$return[22]</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class='table table-striped'>
                                        <thead>
                                            <tr>
                                                <th colspan='4' class='styled'>Service Info</th>
                                            </tr><tr>
                                                <th class='styled'>ID</th>
                                                <th class='styled'>Type</th>
                                                <th class='styled'>Started</th>
                                                <th class='styled'>$finishedTitle</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>$return[0]</td>
                                                <td>$otherDetails</td>
                                                <td>$return[5]</td>
                                                <td>$finishDate</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class='table table-striped' style='margin-bottom: 50px;'>
                                        <thead>
                                            <tr>
                                                <th colspan='4' class='styled'>Service Parts</th>
                                            </tr><tr>
                                                <th class='styled'>ID</th>
                                                <th class='styled'>Name</th>
                                                <th class='styled'>Price</th>
                                                <th class='styled'>-</th>
                                            </tr>
                                        </thead>
                                        <tbody>";
        
                                        while($partsRow = mysqli_fetch_array($partsQuery)){
                                            print "<tr>"
                                            . "<td>$partsRow[0]</td>"
                                            . "<td>$partsRow[1]</td>"
                                            . "<td>£$partsRow[2]</td>"
                                            . "<td><button class='btn btn-primary' data-toggle='modal' href='#deletePart$partsRow[0]'>Remove Part</button></td>"
                                            . "</tr>";
                                        }
                                        print "</tbody>
                                            </table>
                                            <button class='btn btn-primary' style='position: absolute; width: 120px; bottom: 5px; right: 5px;' data-toggle='modal' href='#addPart$return[0]'>New Part</button>
                                            <button class='btn btn-primary' style='position: absolute; width: 120px; bottom: 5px; left: 5px;' data-toggle='modal' href='#editService$return[0]'>Edit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>";
                                        
$partsCommand = "SELECT * FROM $partsTable WHERE Service_ID = $return[0]";
$partsQuery2 = mysqli_query($connection, $partsCommand) or die(mysqli_error());

while($partsRow = mysqli_fetch_array($partsQuery2)){
    print "<div class='modal fade' id='deletePart$partsRow[0]' tabindex='-1' role='dialog' aria-labelledby='deletePart$partsRow[0]' aria-hidden='true'>

            <div class='modal-dialog modal-sm'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                        <p class='modal-title'>Are you sure?</p>
                    </div>
                    <div class='modal-body' style='text-align: left;'>
                        <form action='' method='post' name='deletePartForm'>
                            <input type='hidden' name='partID' value='$partsRow[0]'/>
                            <input type='hidden' name='serviceID' value='$return[0]'/>
                            <button class='btn btn-primary' style='margin: 10px 0px; width: 270px;' type='submit' name='DeletePart'>Delete Part?</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>";
                                }
                                        
print "<div class='modal fade' id='addPart$return[0]' tabindex='-1' role='dialog' aria-labelledby='addPart$return[0]' aria-hidden='true'>
            <div class='modal-dialog modal-sm'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                        <p class='modal-title'>New Part</p>
                    </div>
                    <div class='modal-body'>
                        <form class='form-signin' name='addPartForm' method='post' action=''>
                            <input class='form-control form-styling' type='text' placeholder='Part Name' name='name' required>
                            <input class='form-control form-styling' type='number' placeholder='Part Price(£)' name='price' required>
                            <input type='hidden' name='serviceID' value='$return[0]' required>
                            <button class='btn btn-primary form-styling' type='submit' name='addPart'>Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>";
                                
print "<div class='modal fade' id='editService$return[0]' tabindex='-1' role='dialog' aria-labelledby='editService$return[0]' aria-hidden='true'>

            <div class='modal-dialog modal-sm'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                        <p class='modal-title'>Edit Service:</p>
                    </div>
                    <div class='modal-body' style='text-align: left;'>
                        <button class='btn btn-primary' style='margin: 10px 0px; width: 270px;' data-toggle='modal' href='#editCar$return[0]'>Change Car</button></br>
                        <button class='btn btn-primary' style='margin: 10px 0px; width: 270px;' data-toggle='modal' href='#editType$return[0]'>Change Type</button>
                        <button class='btn btn-primary' style='margin: 10px 0px; width: 270px;' data-toggle='modal' href='#delete$return[0]'>Delete Service</button>
                    </div>
                </div>
            </div>
        </div>";

print "<div class='modal fade' id='delete$return[0]' tabindex='-1' role='dialog' aria-labelledby='delete$return[0]' aria-hidden='true'>

            <div class='modal-dialog modal-sm'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                        <p class='modal-title'>Are you sure?</p>
                    </div>
                    <div class='modal-body' style='text-align: left;'>
                        <form action='' method='post' name='deleteForm'>
                            <input type='hidden' name='serviceID' value='$return[0]'/>
                            <button class='btn btn-primary' style='margin: 10px 0px; width: 270px;' type='submit' name='DeleteService'>Delete Service?</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>";

print "<div class='modal fade' id='editType$return[0]' tabindex='-1' role='dialog' aria-labelledby='editType$return[0]' aria-hidden='true'>

            <div class='modal-dialog modal-sm'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                        <p class='modal-title'>Edit Service:</p>
                    </div>
                    <div class='modal-body' style='text-align: left;'>
                        <form class='form-signin' name='editServiceForm' action='' method='post'>
                        
                        <script>
                            $(document).ready(function () {
                                toggleFields$return[0]();
                                $('#editServiceType$return[0]').change(function () {
                                    toggleFields$return[0]();
                                });
                            });
                                        
                            function toggleFields$return[0]() {
                                if ($('#editServiceType$return[0]').val() == 'Other')
                                    $('#other$return[0]').show();
                                else
                                    $('#other$return[0]').hide();
                            }
                        </script>

                            <input class='login-input form-styling' type='hidden' name='serviceID' value='$return[0]' readonly>
                            <select class='form-control form-styling' id='editServiceType$return[0]' name='editServiceType' required>
                                <option value='MOT'>MOT</option>
                                <option value='Tire Change'>Tyre Change</option>
                                <option value='Internal Electronics'>Internal Electronics</option>
                                <option value='Engine Repair'>Engine Repair</option>
                                <option value='Other'>Other</option>
                            </select>
                            <div id='other$return[0]'>
                                <input class='form-control form-styling' type='text' name='OtherInput' placeholder='Other'>
                            </div>
                            <button class='btn btn-primary' type='submit' name='ServiceEdit'>Edit Service</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>";

print "<div class='modal fade' id='editCar$return[0]' tabindex='-1' role='dialog' aria-labelledby='editCar$return[0]' aria-hidden='true'>
            <div class='modal-dialog modal-sm'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                        <p class='modal-title'>Search For Car By:</p>
                    </div>
                    <div class='modal-body'>
                        <button class='btn btn-primary' data-toggle='modal' href='#searchCustomerEdit$return[0]'>Customer</button>
                        <button class='btn btn-primary' data-toggle='modal' href='#searchRegEdit$return[0]'>Registration</button>
                    </div>
                </div>
            </div>
        </div>

        <div class='modal fade' id='searchCustomerEdit$return[0]' tabindex='-1' role='dialog' aria-labelledby='searchCustomerEdit$return[0]' aria-hidden='true'>
            <div class='modal-dialog modal-sm'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                        <p class='modal-title'>Search For Cars Owned By:</p>
                    </div>
                    <div class='modal-body'>
                        <form class='form-signin' name='editCustomerForm' method='post' action=''>
                            <input class='login-input' type='text' name='editServicesCustName' required>
                            <input type='hidden' name='serviceID' value='$return[0]'>
                            <button style='margin-top: -2px;' class='btn btn-primary' type='submit'>Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class='modal fade' id='searchRegEdit$return[0]' tabindex='-1' role='dialog' aria-labelledby='searchRegEdit$return[0]' aria-hidden='true'>
            <div class='modal-dialog modal-sm'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                        <p class='modal-title'>Search For Cars By Registration:</p>
                    </div>
                    <div class='modal-body'>
                        <form class='form-signin' name='searchRegForm' method='post' action=''>
                            <input class='login-input' type='text' name='editServicesReg' required>
                            <input type='hidden' name='serviceID' value='$return[0]'>
                            <button style='margin-top: -2px;' class='btn btn-primary' type='submit'>Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>";
    }
}
?>
                </center>

                    <?php
                    if ($servicesRangeLow > 0) {
                        $previous = $page - 1;
                        print "<button class='btn btn-primary' style='position: absolute; width: 120px; bottom: 5px; left: 5px;' data-toggle='modal' href='services.php?page=$previous'>Previous Page</button>";
                    } else {
                        print "<button class='btn btn-primary' style='position: absolute; width: 120px; bottom: 5px; left: 5px;' data-toggle='modal' href='' disabled>Previous Page</button>";
                    }

                    if ($rowCount > $servicesRangeHigh) {
                        $next = $page + 1;
                        print "<button class='btn btn-primary' style='position: absolute; width: 120px; bottom: 5px; left: 130px;' data-toggle='modal' href='services.php?page=$next'>Next Page</button>";
                    } else {
                        print "<button class='btn btn-primary' style='position: absolute; width: 120px; bottom: 5px; left: 130px;' data-toggle='modal' href='' disabled>Next Page</button>";
                    }

                    if (isset($_POST['searchServicesCustName']) || isset($_POST['searchServicesReg'])) {
                        print "<form action=''><button class='btn btn-primary' style='position: absolute; width: 170px; bottom: 5px; right: 5px;' type='submit'>Back</button></form>";
                    } else {
                        print "<button class = 'btn btn-primary' style = 'position: absolute; width: 170px; bottom: 5px; right: 5px;' data-toggle = 'modal' href = '#searchBy'>New Service</button>";
                    }
                    ?>
            </div>
        </div>

        <div class="modal fade" id="logout" tabindex="-1" role="dialog" aria-labelledby="logout" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <p class="modal-title">Are you sure you want to logout?</p>
                    </div>
                    <div class="modal-body">
                        <div style="text-align: center;">
                            <form method="post" action="internal_data/functions/logout.php">
                                <button class="btn btn-primary" type="submit" name="logout">Logout</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class='modal fade' id='searchBy' tabindex='-1' role='dialog' aria-labelledby='searchBy' aria-hidden='true'>
            <div class='modal-dialog modal-sm'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                        <p class='modal-title'>Search For Car By:</p>
                    </div>
                    <div class='modal-body'>
                        <button class='btn btn-primary' data-toggle='modal' href='#searchCustomer'>Customer</button>
                        <button class='btn btn-primary' data-toggle='modal' href='#searchReg'>Registration</button>
                    </div>
                </div>
            </div>
        </div>

        <div class='modal fade' id='searchCustomer' tabindex='-1' role='dialog' aria-labelledby='searchCustomer' aria-hidden='true'>
            <div class='modal-dialog modal-sm'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                        <p class='modal-title'>Search For Cars Owned By:</p>
                    </div>
                    <div class='modal-body'>
                        <form class='form-signin' name='searchCustomerForm' method='post' action=''>
                            <input class='login-input' type='text' name='searchServicesCustName' required>
                            <button style='margin-top: -2px;' class='btn btn-primary' type='submit'>Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class='modal fade' id='searchReg' tabindex='-1' role='dialog' aria-labelledby='searchReg' aria-hidden='true'>
            <div class='modal-dialog modal-sm'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                        <p class='modal-title'>Search For Cars By Registration:</p>
                    </div>
                    <div class='modal-body'>
                        <form class='form-signin' name='searchRegForm' method='post' action=''>
                            <input class='login-input' type='text' name='searchServicesReg' required>
                            <button style='margin-top: -2px;' class='btn btn-primary' type='submit'>Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php
            if(isset($_POST['addPart']) || isset($_POST['DeletePart'])){
                print "<script type='text/javascript'>
                            $(window).load(function(){
                                $('#sid$serviceID').modal('show');
                            });
                        </script>";
            }
        ?>
    </body>
</html>