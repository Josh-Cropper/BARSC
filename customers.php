modalise<?php
include_once('internal_data/_settings.php');
include_once('internal_data/_connect.php');
include_once 'internal_data/functions/coreFunctions.php';

if (isset($_SESSION['Username'])) {

    $servicesTable = $_SETTINGS['SERVICES_TABLE'];
    $customersTable = $_SETTINGS['CUSTOMERS_TABLE'];
    $carsTable = $_SETTINGS['CARS_TABLE'];

    $servicesRangeLow = 0;
    $servicesRangeHigh = 20;

    $rowCount = 0;

    if (isset($_POST['addCustomerSubmit'])) {

        $name = $_POST['name'];
        $number = $_POST['number'];
        $address1 = $_POST['address1'];
        $address2 = $_POST['address2'];
        $postcode = $_POST['postcode'];
        $city = $_POST['city'];
        $county = $_POST['county'];
        $DOB = $_POST['DOB'];

        $command = "INSERT INTO $customersTable (Name, Address, Address_2, Postcode, City, County, DOB, Phone_No) VALUES ('$name', '$address1', '$address2', '$postcode', '$city', '$county', '$DOB', '$number');";

        $query = mysqli_query($connection, $command) or die(mysqli_error());
    }

    if (isset($_POST['page'])) {
        $servicesRangeLow = ($_POST['page'] * 20) - 20;
        $servicesRangeHigh = $_POST['page'] * 20;
    }

    $command = "SELECT * FROM $customersTable ORDER BY Customer_ID LIMIT $servicesRangeLow, $servicesRangeHigh;";

    if (isset($_POST['searchValue'])) {
        $search = $_POST['searchValue'];

        $command = "SELECT $customersTable.*, $carsTable.Car_ID, $carsTable.Registration_No FROM $customersTable"
                . " LEFT JOIN $carsTable ON $carsTable.Customer_ID = $customersTable.Customer_ID"
                . " WHERE $customersTable.Name LIKE '%$search%' OR $carsTable.Registration_No = '$search'"
                . " ORDER BY $customersTable.Customer_ID;";
    } else if(isset($_POST['editCustomerSubmit'])){
        $customerID = $_POST['customerID'];
        $name = $_POST['name'];
        $number = $_POST['number'];
        $address1 = $_POST['address1'];
        $address2 = $_POST['address2'];
        $postcode = $_POST['postcode'];
        $city = $_POST['city'];
        $county = $_POST['county'];
        $DOB = $_POST['DOB'];
        
        $command = "UPDATE $customersTable SET Name='$name', Address='$address1', Address_2='$address2',"
                . " Postcode='$postcode',  City='$city', County='$county', DOB='$DOB', Phone_No='$number' WHERE Customer_ID='$customerID';";
        $query = mysqli_query($connection, $command) or die(mysqli_error());
        
        $command = "SELECT $customersTable.*, $carsTable.Car_ID, $carsTable.Registration_No FROM $customersTable"
                . " LEFT JOIN $carsTable ON $carsTable.Customer_ID = $customersTable.Customer_ID"
                . " WHERE $customersTable.Customer_ID = '$customerID'"
                . " ORDER BY $customersTable.Customer_ID;";
    } else if(isset($_POST['DeleteCustomer'])){
        $customerID = $_POST['customerID'];
        
        $command = "DELETE FROM $customersTable WHERE Customer_ID='$customerID';";
        $query = mysqli_query($connection, $command) or die(mysqli_error());
        
        $command = "SELECT * FROM $customersTable ORDER BY Customer_ID LIMIT $servicesRangeLow, $servicesRangeHigh;";
    }

    $query = mysqli_query($connection, $command) or die(mysqli_error());

    $rowCount = mysqli_num_rows($query);
} else {
    header("Location: index.php");
}
?>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" type="text/css" href="design/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="design/css/style.css">
        <link rel="stylesheet" type="text/css" href="design/css/datepicker.css">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="design/css/js/bootstrap.min.js"></script>
        <script src="design/css/js/bootstrap-datepicker.js"></script>

        <title>BARSC Customers</title>
    </head>
    <body>
        <?php
        print returnSidebar("customers");
        ?>
        <div class="content-container">
            <div class="main-content" style="text-align: center;">
                <h1 class="content-title">Customers</h1>

                <?php
                $previous = array();
                while ($return = mysqli_fetch_array($query)) {
                    if (in_array($return[0], $previous)) {
                        
                    } else {
                        array_push($previous, $return[0]);
                        $carsCommand = "SELECT * FROM $carsTable WHERE Customer_ID = '$return[0]';";
                        $carsQuery = mysqli_query($connection, $carsCommand) or die(mysqli_error());
                        $servicesCommand = "SELECT * FROM $servicesTable WHERE Customer_ID = '$return[0]';";
                        $servicesQuery = mysqli_query($connection, $servicesCommand) or die(mysqli_error());
                        print "<div class='inner-content-block'>
                            <h2 class='inner-content-title'>$return[1]</h2>
                                <p class='inner-content-text'><strong>DOB: </strong>$return[2]</p>
                                <p class='inner-content-text'><strong>Phone Number: </strong>$return[3]</p>
                                <p style='position: absolute; top: 11px; right: 10px; color: white;'>ID: $return[0]</p>
                                <button class='btn btn-primary' style='width: 240px; position: absolute; bottom: -5px; left: 50%; transform: translateX(-50%);' data-toggle='modal' href='#sid$return[0]'>More Info</button></div>";

                        print " <div class='modal fade' id='sid$return[0]' tabindex='-1' role='dialog' aria-labelledby='moreInfo' aria-hidden='true' >
                                    <div class='modal-dialog modal-sm' style='width: 800px;'>
                                        <div class='modal-content'>
                                            <div class='modal-header'>
                                                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                                                <p class='modal-title'>Customer $return[0] More Information</p>
                                            </div>
                                            <div class='modal-body' >
                                                <table class='table table-striped'>
                                        <thead>
                                            <tr>
                                                <th colspan='5' class='styled'>Customer</th>
                                            </tr><tr>
                                                <th class='styled'>ID</th>
                                                <th class='styled'>Name</th>
                                                <th class='styled'>Date of Birth</th>
                                                <th class='styled'>Address</th>
                                                <th class='styled'>Phone Number</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>$return[0]</td>
                                                <td>$return[1]</td>
                                                <td>$return[2]</td>
                                                <td>$return[4],</br>". ($return[5] != null ? "$return[5],</br>" : "") ."$return[7],</br> $return[8],</br> $return[6]</td>
                                                <td>$return[3]</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class='table table-striped'>
                                        <thead>
                                            <tr>
                                                <th colspan='6' class='styled'>Cars</th>
                                            </tr><tr>
                                                <th class='styled'>ID</th>
                                                <th class='styled'>Registration</th>
                                                <th class='styled'>Make</th>
                                                <th class='styled'>Model</th>
                                                <th class='styled'>Colour</th>
                                                <th class='styled'>-</th>
                                            </tr>
                                        </thead>
                                        <tbody>";
                        while($cars = mysqli_fetch_array($carsQuery)){
                            print "<tr>"
                                            . "<td>$cars[0]</td>"
                                            . "<td style='text-transform: uppercase;'>$cars[2]</td>"
                                            . "<td>$cars[3]</td>"
                                            . "<td>$cars[4]</td>"
                                            . "<td>$cars[5]</td>"
                                            . "<td><form action='cars.php' method='post'>"
                . "<input type='hidden' name='searchValue' value='$cars[2]'>"
                . "<button class='btn btn-primary' type='submit' name='submitButton'>Go To</button>"
                . "</form></td></tr>";
                        }
                                    print "</tbody>
                                    </table>
                                    <table class='table table-striped' style='margin-bottom: 40px;'>
                                        <thead>
                                            <tr>
                                                <th colspan='4' class='styled'>Services</th>
                                            </tr><tr>
                                                <th class='styled'>ID</th>
                                                <th class='styled'>Type</th>
                                                <th class='styled'>Started</th>
                                                <th class='styled'>-</th>
                                            </tr>
                                        </thead>
                                        <tbody>";
                                    while($services = mysqli_fetch_array($servicesQuery)){
                                        $type = $services[3] == "Other" ? "$services[3]: $services[4]" : $services[3];
                                        print "<tr>"
                                            . "<td>$services[0]</td>"
                                            . "<td>$type</td>"
                                            . "<td>$services[5]</td>"
                                            . "<td><form action='services.php' method='post'>"
                                            . "<input type='hidden' name='searchValue' value='$services[0]'>"
                                            . "<button class='btn btn-primary' type='submit' name='submitButton'>Go To</button>"
                                            . "</form></td></tr>";
                                    }
                                        print "</tbody>
                                    </table>
                                            <button class='btn btn-primary' style='position: absolute; width: 120px; bottom: 5px; left: 5px;' data-toggle='modal' href='#editCust$return[0]'>Edit</button>
                                            <button class='btn btn-primary' style='position: absolute; width: 120px; bottom: 5px; right: 5px;' data-toggle='modal' href='#delete$return[0]'>Delete</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>"
                                                . ""
                                                . "<div class='modal fade' id='editCust$return[0]' tabindex='-1' role='dialog' aria-labelledby='editCust$return[0]' aria-hidden='true'>
            <div class='modal-dialog modal-sm'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                        <p class='modal-title'>Edit Customer</p>
                    </div>
                    <div class='modal-body'>
                        <form class='form-signin' name='editCustomerForm' method='post' action=''>
                            <input type='hidden' name='customerID' value='$return[0]'>
                            <input class='form-control form-styling' type='text' name='name' placeholder='John Doe' value='$return[1]' required>
                            <input class='form-control form-styling' type='text' name='address1' placeholder='Address Line 1' value='$return[4]' required>
                            <input class='form-control form-styling' type='text' name='address2' placeholder='Address Line 2' value='$return[5]'>
                            <input class='form-control form-styling' type='text' name='city' placeholder='City' value='$return[7]' required>
                            <input class='form-control form-styling' type='text' name='county' placeholder='County' value='$return[8]' required>
                            <input class='form-control form-styling' type='text' name='postcode' placeholder='Postcode' value='$return[6]' required>
                            <input class='form-control form-styling' type='text' name='number' placeholder='Phone Number' value='$return[3]' maxlength='11' required>
                            <input type='text' class='form-control' name='DOB' data-provide='datepicker' data-date-format='yyyy-mm-dd' placeholder='YYYY-MM-DD' value='$return[2]' required>
                            <button style='margin-top: 10px;' class='btn btn-primary' type='submit' name='editCustomerSubmit'>Edit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>";
                                        
                print "<div class='modal fade' id='delete$return[0]' tabindex='-1' role='dialog' aria-labelledby='delete$return[0]' aria-hidden='true'>

            <div class='modal-dialog modal-sm'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                        <p class='modal-title'>Are you sure?</p>
                    </div>
                    <div class='modal-body' style='text-align: left;'>
                        <form action='' method='post' name='deleteForm'>
                            <input type='hidden' name='customerID' value='$return[0]'/>
                            <button class='btn btn-primary' style='margin: 10px 0px; width: 270px;' type='submit' name='DeleteCustomer'>Delete Customer?</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>";
                    }
                }
                ?>


                <?php
                if ($servicesRangeLow > 0) {
                    print "<button class='btn btn-primary' style='position: absolute; width: 120px; bottom: 5px; left: 5px;' data-toggle='modal' href=''>Previous Page</button>";
                } else {
                    print "<button class='btn btn-primary' style='position: absolute; width: 120px; bottom: 5px; left: 5px;' data-toggle='modal' href='' disabled>Previous Page</button>";
                }

                if ($rowCount > $servicesRangeHigh) {
                    print "<button class='btn btn-primary' style='position: absolute; width: 120px; bottom: 5px; left: 130px;' data-toggle='modal' href=''>Next Page</button>";
                } else {
                    print "<button class='btn btn-primary' style='position: absolute; width: 120px; bottom: 5px; left: 130px;' data-toggle='modal' href='' disabled>Next Page</button>";
                }
                ?>

                <button class='btn btn-primary' style='position: absolute; width: 170px; bottom: 5px; right: 5px;' data-toggle='modal' href='#addCustomer'>New Customer</button>
            </div>
        </div>

        <div class='modal fade' id='addCustomer' tabindex='-1' role='dialog' aria-labelledby='addCustomer' aria-hidden='true'>
            <div class='modal-dialog modal-sm'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                        <p class='modal-title'>Add Customer</p>
                    </div>
                    <div class='modal-body'>
                        <form class='form-signin' name='addCustomerForm' method='post' action=''>
                            <input class='form-control form-styling' type='text' name='name' placeholder='John Doe' required>
                            <input class='form-control form-styling' type='text' name='address1' placeholder='Address Line 1' required>
                            <input class='form-control form-styling' type='text' name='address2' placeholder='Address Line 2'>
                            <input class='form-control form-styling' type='text' name='city' placeholder='City' required>
                            <input class='form-control form-styling' type='text' name='county' placeholder='County' required>
                            <input class='form-control form-styling' type='text' name='postcode' placeholder='Postcode' required>
                            <input class='form-control form-styling' type='text' name='number' placeholder='Phone Number' maxlength='11' required>
                            <input type='text' class='form-control' name='DOB' data-provide='datepicker' data-date-format='yyyy-mm-dd' placeholder='YYYY-MM-DD' required>
                            <button style='margin-top: 10px;' class='btn btn-primary' type='submit' name='addCustomerSubmit'>Add</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="logout" tabindex="-1" role="dialog" aria-labelledby="logout" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <p class="modal-title">Are you sure you want to logout?</p>
                    </div>
                    <div class="modal-body">
                        <div style="text-align: center;">
                            <form method="post" action="internal_data/functions/logout.php">
                                <button class="btn btn-primary" type="submit" name="logout">Logout</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </body>
    <footer>



    </footer>
</html>